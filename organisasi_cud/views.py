from django.http import HttpResponse
from django.shortcuts import render, redirect
from organisasi_cud.database import insert_organisasi, get_organisasi, delete_organisasi, get_one_organisasi
from organisasi_cud.forms import OrganisasiForm

# Create your views here.
def index(request):
    organisasi = get_organisasi()
    return render(request, 'organisasi_cud.html', {'orgs':organisasi})

def organisasi_delete(request, id_organisasi):
    delete_organisasi(id_organisasi)
    return redirect('organisasi-create')

def form_cud(request):
    if request.method == 'POST':
        form = OrganisasiForm(request.POST)
        if form.is_valid():
            nama = request.POST['nama']
            tingkatan = request.POST['tingkatan']
            kategori = request.POST['kategori']
            email = request.POST['email']
            narahubung = request.POST['contact_person']
            web = request.POST['alamat_website']
            deskripsi = request.POST['deskripsi']

            insert_organisasi(nama=nama,
                 email=email,
                 alamat_website=email,
                 tingkatan=tingkatan,
                 kategori=kategori,
                 deskripsi=deskripsi,
                 contact_person=narahubung)
            return redirect('organisasi')
        else:
            return HttpResponse("form is not valid")

    form = OrganisasiForm()
    organisasi = get_organisasi()
    if request.session['role'] == "admin":
        return render(request, 'form_cud_organisasi.html', {'form': form, 'orgs':organisasi})
    else:
        return render(request, 'form_update_organisasi.html', {'form': form, 'orgs':organisasi})

def form_update(request, id_organisasi):
    org = get_one_organisasi(id_organisasi)
    form = OrganisasiForm(
        initial={'nama':org[1],
                 'email':org[2],
                 'alamat_website':org[3],
                 'tingkatan':org[4],
                 'kategori':org[5],
                 'deskripsi':org[7],
                 'contact_person':org[8]
                 }
    )
    response = render(request, 'form_update_organisasi.html', {'form': form})
    response.set_cookie('id_organisasi',id_organisasi)
    return response

def organisasi_update_post(request):
    if request.method == 'POST':
        form = OrganisasiForm(request.POST)
        if form.is_valid():
            nama = request.POST['nama']
            tingkatan = request.POST['tingkatan']
            kategori = request.POST['kategori']
            email = request.POST['email']
            narahubung = request.POST['contact_person']
            web = request.POST['alamat_website']
            deskripsi = request.POST['deskripsi']

            delete_organisasi(request.COOKIES['id_organisasi'])
            insert_organisasi(nama=nama,
                 email=email,
                 alamat_website=email,
                 tingkatan=tingkatan,
                 kategori=kategori,
                 deskripsi=deskripsi,
                 contact_person=narahubung)
            return redirect('organisasi')
        else:
            return HttpResponse("form is not valid")
    return redirect('organisasi')
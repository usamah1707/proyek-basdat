from django.urls import path

from event_cud.views import event_index, event_reg, event_detail, event_delete, event_update, \
    event_update_post

urlpatterns = [
    path('', event_index, name='event'),
    path('create', event_reg, name='event-create'),
    path('delete/<int:id_event>/<str:id_pembuat_event>/', event_delete, name='event_delete'),
    path('update/<int:id_event>/<str:id_pembuat_event>/', event_update, name='event_update'),
    path('update/post', event_update_post, name='event-update-post'),
    path('detail', event_detail, name='event-detail'),
]

from django.shortcuts import render

# Create your views here.
def kepanitiaan(request):
    return render(request, 'kepanitiaan_cud.html')

def create_kepanitiaan(request):
    return render(request, 'form_create_kepanitiaan.html')

def update_kepanitiaan(request):
    return render(request, 'form_update_kepanitiaan.html')
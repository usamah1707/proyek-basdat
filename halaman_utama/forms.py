from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(
        label='Username',
        max_length=15,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    password = forms.CharField(
        label='Password',
        max_length=50,
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control'
            }
        ))


class DaftarForm(forms.Form):
    nama = forms.CharField(
        label='Nama',
        max_length=50,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    email = forms.CharField(
        label='Email',
        max_length=50,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    password = forms.CharField(
        label='Password',
        max_length=20,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    username = forms.CharField(
        label='Username',
        max_length=15,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    password = forms.CharField(
        label='Password',
        max_length=50,
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control'
            }
        ))
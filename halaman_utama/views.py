from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.contrib import auth
from halaman_utama.forms import LoginForm, DaftarForm
from halaman_utama.database import cekLogin, cekRole


def home(request):
    request.session['role'] = "nonadmin" #default
    if request.session.has_key('username'):
       request.session['role'] = cekRole(request.session['username']) #gatau ini masih perlu atau engga
    return render(request, 'halaman_utama.html')



def login(request):
    request.session['role'] = "nonadmin" #default
    loginform = LoginForm()
    daftarform = DaftarForm()
    username = None  # default value
    if request.method == 'GET':
 
        if 'action' in request.GET:
            action = request.GET.get('action')
            if action == 'logout':
                if request.session.has_key('username'):
                    request.session.flush()
                return redirect('login')
 
        if 'username' in request.session:
            username = request.session['username']
            print(request.session.get_expiry_age())  # session lifetime in seconds(from now)
            print(request.session.get_expiry_date())  # datetime.datetime object which represents the moment in time at which the session will expire
 
    elif request.method == 'POST':
        loginform = LoginForm(request.POST)
        if loginform.is_valid():
            username = loginform.cleaned_data['username']
            password = loginform.cleaned_data['password']
            if cekLogin(username.strip()) is None:
                username = None
            
            else:
                if username.strip() == cekLogin(username.strip())[0]:
                    if password.strip() == cekLogin(username.strip())[1]:
                        request.session['username'] = username
                        request.session['role'] = cekRole(username)          
    return render(request, 'login.html',  {'loginform': loginform, 'daftarform': daftarform, 'username': username})
    

def logout(request):
	print("non")
	# auth.logout(request)
	# messages.info(request, 'Anda berhasil keluar')
	# return redirect(reverse('index'))
